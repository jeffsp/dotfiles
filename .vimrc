" Reload .vimrc on write
autocmd! bufwritepost .vimrc source %

" Load theme
set background=dark
colorscheme solarized8

" Add new leader, default is '\'
let mapleader = ","

" Remember undo's across edit sessions
set undofile
set undodir=~/.vim/undodir
set undolevels=10000 "maximum number of changes that can be undone
set undoreload=100000 "maximum number lines to save for undo on a buffer reload

" Remember info about open buffers on close
set viminfo^=%

" Run a terminal
noremap <leader>t :terminal<cr>

" Change buffers
noremap <leader>l :bnext<cr>
noremap <leader>h :bprevious<cr>

" Close buffer
noremap <leader>x :bd<cr>

" Go to next error
noremap <leader>n :cnext<cr>

" Go to previous error
noremap <leader>p :cprevious<cr>

" Go to first error
noremap <leader>1 :cfirst<cr>

" Run make
noremap <leader>m :make<cr>

" Autowrite
set aw

" Show numbers
set number

" Assume case insensitive...
set ignorecase

" ... unless an uppercase is used in search string
set smartcase

" Highlight trailing spaces and tabs
highlight RedundantSpaces ctermbg=red
match RedundantSpaces /\s\+$\|\t\|\t\+$/

" Highlight columns > 80
let &colorcolumn="72,120"

" C++ indenting
autocmd FileType c,cc,cpp :set cindent

" Tabs
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" Enable filetype plugins
filetype plugin on
filetype indent on

" Parse errors from make
set errorformat =%E%f:%l:%c:\ %trror:\ %m,%-Z%p^,%+C%.%#
set errorformat+=%D%*\\a:\ Entering\ directory\ [`']%f'
set errorformat+=%X%*\\a:\ Leaving\ directory\ [`']%f'
set errorformat+=%-G%.%#

" Setup ALE
let g:ale_linters = {'cpp': ['cppcheck', 'g++']}
