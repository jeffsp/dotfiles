alias ls='ls -F --color'
alias lsa='ls -altrhF --color'
alias df='df -h'
alias grep='grep -d skip --color=auto'
alias h='history'
alias vim='nvim'
alias vi='nvim'
alias gs="git status"
alias gd="git diff"

export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
set PATH /usr/lib/ccache/bin $PATH
set PATH /usr/local/cuda-12.2/bin $PATH
status --is-login; and status --is-interactive; and exec byobu-launcher
fish_vi_key_bindings
set EDIT vim
