" Use VIM's path instead of NeoVIM's path
set runtimepath^=~/.vim

" Tell NeoVIM where it's package manager can find plugins
let &packpath=&runtimepath

" Load the configuration
source ~/.vimrc
