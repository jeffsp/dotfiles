# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# User specific aliases and functions
[ -r /home/perryjs1/.byobu/prompt ] && . /home/perryjs1/.byobu/prompt   #byobu-prompt#

alias ls='ls -F --color'
alias lsa='ls -altrhF --color'
alias df='df -h'
alias grep='grep -d skip --color=auto'
alias h='history'
alias vim='nvim'
alias vi='nvim'
alias gs="git status"
alias gd="git diff"

source /opt/rh/gcc-toolset-12/enable
